﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace MailHelper
{
    public static class MailConst
    {
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static string SmtpServer { get; set; }
    }
    public class Mailer
    {

        public static void SendMail(string examname, string username, string email)
        {
            using(MailMessage mail = new MailMessage("erpadmin@kaizentechnosoft.in", email))
            {
                SmtpClient client = new SmtpClient();
                client.Port = 3535;
                //client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("erpadmin@kaizentechnosoft.in", "Kaizen!@123");
                client.Host = "smtpout.asia.secureserver.net";
                mail.Subject = "this is a test email.";
                mail.IsBodyHtml = true;
                mail.Body = "<div style=\"background-color: #eee\"><h1>Test Mail</h1><br/><p>This is test mail send from test enviroment.</p></div>";
                client.Send(mail);
            }
        }
        public static void SendMail1(string examname, string username, string email)
        {
            //string filename = HttpContext.Current.Server.MapPath("~/MailAboutExam.html");
            string mailbody = "<h1>Hello World</h1>";
            //mailbody = mailbody.Replace("##UserName##", username);
            //mailbody = mailbody.Replace("##ExamName##", examname);
            string from = "tamilselvan719@hotmail.com";
            string to = email;
            using (MailMessage message = new MailMessage(from, to))
            {
                message.Subject = "kaizen Online Exam";
                message.Body = mailbody;
                message.BodyEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("pop.asia.secureserver.net", 110);
                NetworkCredential basicCredential = new NetworkCredential("erpadmin@kaizentechnosoft.in", "Kaizen!@123");
                client.EnableSsl = false;
                client.UseDefaultCredentials = true;
                client.Credentials = basicCredential;
                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public static void SendMail(IEnumerable<string> recipients, string subject, string body, IEnumerable<string> attachmentFilenames, string cc = "", string bcc = "")
        {
            MailConst.Username = "clinictest@kaizentechnosoft.com";
            MailConst.Password = "CliniC!@123";
            MailConst.SmtpServer = "webmail.kaizentechnosoft.com";

            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredential = new NetworkCredential(MailConst.Username, MailConst.Password);
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress(MailConst.Username);

            // setup up the host, increase the timeout to 5 minutes
            smtpClient.Host = MailConst.SmtpServer;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = basicCredential;
            smtpClient.Timeout = (60 * 5 * 1000);

            message.From = fromAddress;
            message.Subject = subject;
            message.IsBodyHtml = false;
            message.Body = body;

            if (cc != "")
                message.CC.Add(cc);
            if (bcc != "")
                message.Bcc.Add(bcc);

            foreach (var recipient in recipients)
                message.To.Add(recipient);

            foreach(var attachmentFilename in attachmentFilenames){
                if (attachmentFilename != null)
                {
                    Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                    disposition.FileName = Path.GetFileName(attachmentFilename);
                    disposition.Size = new FileInfo(attachmentFilename).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
            }
            smtpClient.Send(message);
        }
    }
}