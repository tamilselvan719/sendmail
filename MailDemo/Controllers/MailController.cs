﻿using MailHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MailDemo.Controllers
{
    public class MailDTO
    {
        public string MailId { get; set; }
    }

    public class MailController : ApiController
    {
        public string Mail(MailDTO mailDetail)
        {
            try
            {
                Mailer.SendMail("Test", "Test", mailDetail.MailId);
                return "Mail successfully sent";
            }
            catch(Exception ex)
            {
                return ex.ToString();
            }
        }

        void sendMailWithAttachment()
        {
            //string msg = "Hai, this is a test";
            //string fileUrl = @"e:\documents\visual studio 2013\Projects\SendingMailWithAttachements\SendingMailWithAttachements\Attachments\image.jpg";
            //string fileUrl1 = @"e:\documents\visual studio 2013\Projects\SendingMailWithAttachements\SendingMailWithAttachements\Attachments\IMG_20160701_193530 (2).jpg";

            //List<string> files = new List<string>();
            //files.Add(fileUrl);
            //files.Add(fileUrl1);

            //List<string> recipients = new List<string>();
            //recipients.Add("tamilselvan719@hotmail.com");

            //Console.WriteLine("Sending Email...");
            //Mailer.SendMail(recipients, "Test Attachments", msg,
            //    files, "tamilselvan719sw@gmail.com");
            //Console.WriteLine("Mail Sending Completed");
            //Console.ReadKey();
        }
    }    
}
